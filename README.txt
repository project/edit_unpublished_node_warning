CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Edit Unpublished Node Warning does pretty much what it says on the tin.
When a user is viewing or editing an unpublished node this module pops
up with a warning message.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/edit_unpublished_node_warning

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/edit_unpublished_node_warning


REQUIREMENTS
------------

No requirements.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Tom Metcalfe (blacklabel_tom) - https://www.drupal.org/u/blacklabel_tom

This project has been sponsored by:
 * Reason Digital - https://www.drupal.org/reason-digital
   We’re a team of over 40 people who’ve been using our digital expertise
   to tackle big social issues since 2008.
   Charities, social enterprises and companies work with us on digital projects
   that fight issues such as violent crime, drug and alcohol addiction, the food
   poverty crisis, and diseases that define our times – HIV/AIDS, dementia
   and diabetes.
   https://reasondigital.com/
